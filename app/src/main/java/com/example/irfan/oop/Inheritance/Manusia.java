package com.example.irfan.oop.Inheritance;

public class Manusia {
    private String nama;
    private int umur;

    Manusia() {

    }

    Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String tampil() {
        return "nama : " + nama + "/n"
                + "umur : " + umur;
    }
}
