package com.example.irfan.oop.Object;

public class Karyawan {
    private String nama;
    private int umur;
    private double gaji;
    private int lama_kerja;

    Karyawan(String nama, int umur)
    {

    }

    Karyawan(String nama, int umur, double gaji, int lama_kerja) {
        this.nama = nama;
        this.umur = umur;
        this.gaji = gaji;
        this.lama_kerja = lama_kerja;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public double getGaji() {
        return gaji;
    }

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }

    public int getLama_kerja() {
        return lama_kerja;
    }

    public void setLama_kerja(int lama_kerja) {
        this.lama_kerja = lama_kerja;
    }

    public double bonus() {
        double bonus;
        if (lama_kerja < 10) {
            bonus = 100000;
        } else {
            bonus = 200000;
        }
        return bonus;
    }
}
