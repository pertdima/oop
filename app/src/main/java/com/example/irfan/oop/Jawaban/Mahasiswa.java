package com.example.irfan.oop.Jawaban;

public class Mahasiswa extends Manusia{
    private String matakuliah;

    Mahasiswa()
    {

    }

    public String getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(String matakuliah) {
        this.matakuliah = matakuliah;
    }

    @Override
    public String tampil()
    {
        return "Nama : " + getNama() + " " +
                "Matakuliah : "+matakuliah;
    }
}
