package com.example.irfan.oop.Jawaban;

public class Karyawan extends Manusia {
    private String jabatan;
    private double gaji;

    Karyawan()
    {

    }

    Karyawan(String jabatan, double gaji)
    {
        this.setJabatan(jabatan);
        this.setGaji(gaji);
    }


    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public double getGaji() {
        return gaji;
    }

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }

    public double naikGaji(int persentasi)
    {
        return gaji+=(persentasi*gaji)/100;
    }

    @Override
    public String tampil()
    {
        return "Nama : " + getNama() + " " +
                "Jabatan : "+jabatan +
                "Gaji : "+gaji;
    }
}
