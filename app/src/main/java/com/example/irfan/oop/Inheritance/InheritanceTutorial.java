package com.example.irfan.oop.Inheritance;

public class InheritanceTutorial {
    public static void main(String[] args) {
//        Murid murid = new Murid();
//
//        murid.setNama("Andi");
//        murid.setUmur(12);
//        murid.setKelas(6);
//
//
//        System.out.print(murid.tampil());

        Karyawan karyawan = new Karyawan();
        karyawan.setNama("Budi");
        karyawan.setUmur(20);
        karyawan.setGaji(100000);
        karyawan.setLama_kerja(2);

        System.out.print(karyawan.tampil());


        Robot robot = new Robot("Industri", 18, 200000);
        Double efisiensiKaryawan = karyawan.efisiensi(karyawan.getGaji(), karyawan.getLama_kerja());
        Double efisiensiRobot = robot.efisiensi(robot.getPengeluaran(), robot.getLama_kerja());

        if (efisiensiKaryawan < efisiensiRobot) {
            System.out.println("Robot lebih efisien");
        } else if (efisiensiKaryawan > efisiensiRobot) {
            System.out.println("Karyawan lebih efisien");
        } else {
            System.out.println("Performa keduanya sama");
        }
    }
}
