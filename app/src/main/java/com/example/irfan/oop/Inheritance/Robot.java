package com.example.irfan.oop.Inheritance;

public class Robot implements Worker {
    private String tipe;
    private int lama_kerja;
    private int pengeluaran;

    Robot()
    {

    }

    Robot(String tipe,int lama_kerja,int pengeluaran)
    {
        this.setTipe(tipe);
        this.setLama_kerja(lama_kerja);
        this.setPengeluaran(pengeluaran);
    }


    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public int getLama_kerja() {
        return lama_kerja;
    }

    public void setLama_kerja(int lama_kerja) {
        this.lama_kerja = lama_kerja;
    }

    public int getPengeluaran() {
        return pengeluaran;
    }

    public void setPengeluaran(int pengeluaran) {
        this.pengeluaran = pengeluaran;
    }

    @Override
    public double efisiensi(double pengeluaran, int lama_kerja) {
        return pengeluaran*lama_kerja;
    }
}
