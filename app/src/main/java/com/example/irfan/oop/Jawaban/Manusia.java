package com.example.irfan.oop.Jawaban;

public class Manusia {
    private String nama;

    Manusia()
    {

    }

    Manusia(String nama)
    {
        this.nama = nama;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String tampil()
    {
        return "Nama : "+ nama;
    }
}
