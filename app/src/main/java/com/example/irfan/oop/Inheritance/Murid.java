package com.example.irfan.oop.Inheritance;

public class Murid extends Manusia {
    private int kelas;

    Murid() {

    }

    Murid(int kelas) {
        this.kelas = kelas;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }

    @Override
    public String tampil()
    {
        return "nama : " + getNama() + " "
                +"umur : " + getUmur() +
                "kelas : " + kelas;
    }
}
