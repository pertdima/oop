package com.example.irfan.oop.Jawaban;

public class Main {
    public static void main(String[] args) {
        Manusia manusia = new Manusia("David");
        System.out.println(manusia.tampil());

        Karyawan karyawan = new Karyawan();
        karyawan.setNama("Irfan");
        karyawan.setGaji(10000);
        karyawan.setJabatan("Programmer");

        System.out.print(karyawan.tampil());
        karyawan.naikGaji(10);
        karyawan.tampil();
        System.out.print("\n"+karyawan.tampil());

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setNama("Jati");
        mahasiswa.setMatakuliah("ASD");
        System.out.print("\n"+mahasiswa.tampil());

    }
}
