package com.example.irfan.oop.Inheritance;

public class Karyawan extends Manusia implements Worker{
    private double gaji;
    private int lama_kerja;

    Karyawan() {
    }

    public double getGaji() {
        return gaji;
    }

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }

    public int getLama_kerja() {
        return lama_kerja;
    }

    public void setLama_kerja(int lama_kerja) {
        this.lama_kerja = lama_kerja;
    }

    @Override
    public double efisiensi(double pengeluaran, int lama_kerja) {
        return pengeluaran*lama_kerja;
    }
}
